# Incomplete Person API. This is just an example for a possible interface.
class PersonController < ApplicationController
  skip_filter :verify_authenticity_token

  rescue_from Exceptions::APIError do |exception|
    render json: { error: exception.error_name, message: exception.message }
  end

  # GET /person/:name/:relation
  def relations
    person = Person.find_by name: params[:name]

    raise Exceptions::PersonNotFound, 'Could not find person with supplied name' if person.nil?

    if Person::VALID_RELATIONS.include? params[:relation]
      render json: person.send(params[:relation])
    else
      raise Exceptions::UnknownRelation, 'See Person::VALID_RELATIONS for handled relations'
    end
  end

  # GET /person/:name/relation_to/:relation_name
  def relation_to
    person = Person.find_by name: params[:name]

    raise Exceptions::PersonNotFound, 'Could not find person with supplied name' if person.nil?

    relation = Person.find_by name: params[:relation_name]

    raise Exceptions::PersonNotFound, 'Could not find relation with supplied name' if relation.nil?

    relation_chain = person.find_relation relation
    reduced_relation = Relations.reduce relation_chain, relation.sex

    render json: { relation: reduced_relation, chain: relation_chain }
  end

  # GET /mothers_with_most_girl_children
  def mothers_with_most_girl_children
    render json: Person.mothers_with_most_girl_children
  end
end
