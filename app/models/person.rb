class Person < ActiveRecord::Base
  SEX_MALE = 'male'
  SEX_FEMALE = 'female'

  VALID_RELATIONS = %w(spouse parents father mother siblings brothers sisters children sons daughters siblings_in_law
  brothers_in_law sisters_in_law cousins maternal_cousins paternal_cousins grandchildren granddaughters grandsons
  parental_siblings maternal_siblings paternal_siblings aunts uncles maternal_aunts maternal_uncles paternal_aunts paternal_uncles)

  belongs_to :father, class_name: 'Person'
  belongs_to :mother, class_name: 'Person'
  belongs_to :spouse, class_name: 'Person'

  validates_uniqueness_of :name

  scope :male, -> { where(sex: SEX_MALE) }
  scope :female, -> { where(sex: SEX_FEMALE) }

  # Creates a male.
  #
  # TODO: Spec Person.male!
  def self.male!(name, father: nil, mother: nil, spouse: nil)
    create_person! name, SEX_MALE, father, mother, spouse
  end

  # Create a female.
  #
  # TODO: Spec Person.female!
  def self.female!(name, father: nil, mother: nil, spouse: nil)
    create_person! name, SEX_FEMALE, father, mother, spouse
  end

  # Create a person. Set spousal relation, if it is available.
  #
  # TODO: Spec Person.create_person!
  def self.create_person!(name, sex, father, mother, spouse)
    person = create!(name: name, sex: sex, father: father, mother: mother, spouse: spouse)
    spouse.update(spouse: person) if spouse
    person
  end

  def self.birth!(name, sex, mother_name)
    mother = Person.find_by name: mother_name

    # TODO: Check for presence of mother, and mother's spouse.

    create!(name: name, sex: sex, mother: mother, father: mother.spouse)
  end

  # Returns mothers who have the highest number of female children.
  #
  # TODO: Spec Person.mothers_with_most_girl_children
  def self.mothers_with_most_girl_children
    number_of_girl_children = Person.where(sex: SEX_FEMALE).group(:mother_id).count.except(nil)
    max_count = number_of_girl_children.values.max
    mother_ids = number_of_girl_children.select { |key, value| value == max_count }.keys
    Person.find(mother_ids)
  end

  # Let's find relation by inspecting the subject's immediate relations. This will lead into a recursive loop search
  # to find the relational path to the target relative. The recursive loop is optimized through the use of a 'covered'
  # list, and per-lookup exclusion lists.
  #
  # @param relative [Person] Person to whom relation is to be discovered.
  # @param covered [Hash] Names of people who have already been 'scanned'.
  # @param path [Array] List of relation names linking current person to root person.
  # @param exclude [Array] List of relation types to exclude from recursion.
  def find_relation(relative, covered = {}, path = [:self], exclude = [])
    return nil, covered if covered.include? self.name

    covered[self.name] = path

    if self == relative
      if path == [:self]
        return path
      else
        return self, covered
      end
    end

    found = nil

    immediate_relations = {}

    # We'll check only immediate relations. Note how, in addition to the covered list, we'll include an exclude list
    # at each level - this avoids unnecessary traversal since 'this' person will check those relations anyway.
    #
    # Also note that these relations are gender-neutral. We'll decide on gender later on, when dealing with specific
    # people.
    immediate_relations[[:parent, [:spouse, :children]]] = parents unless exclude.include? :parents
    immediate_relations[[:spouse, [:spouse, :children]]] = ([spouse] - [nil]) unless exclude.include? :spouse
    immediate_relations[[:sibling, [:parents, :siblings]]] = siblings unless exclude.include? :siblings
    immediate_relations[[:child, [:parents, :siblings]]] = children unless exclude.include? :children

    immediate_relations.each do |relation, people|
      people.each do |person|
        # Note how the relation being added to the current 'path' is gender-resolved. This is where the gender-agnostic
        # titles written above get converted into gender-aware ones.
        current_path = path.dup << person.relation_with_gender(relation[0])

        found, covered = person.find_relation(relative, covered, current_path, relation[1])
        break if found
      end

      break if found
    end

    if path == [:self]
      if found
        return covered[found.name]
      else
        nil
      end
    else
      return found, covered
    end
  end

  def relation_with_gender(relation_without_gender)
    if female?
      Relations.female_version(relation_without_gender)
    else
      Relations.male_version(relation_without_gender)
    end
  end

  def parents
    [mother, father] - [nil]
  end

  def grandchildren
    children.map(&:children).flatten
  end

  def granddaughters
    grandchildren.select { |child| child.female? }
  end

  def grandsons
    grandchildren.select { |child| child.male? }
  end

  def male?
    sex == SEX_MALE
  end

  def female?
    sex == SEX_FEMALE
  end

  def cousins
    paternal_cousins.merge(maternal_cousins)
  end

  def maternal_cousins
    return Person.none unless mother.present?

    mother.siblings.map(&:children).flatten
  end

  def paternal_cousins
    return Person.none unless father.present?

    father.siblings.map(&:children).flatten
  end

  def siblings
    return Person.none unless mother.present?

    Person.where(mother: mother).where.not(id: id)
  end

  def brothers
    return [] unless mother.present?

    siblings.where(sex: SEX_MALE)
  end

  def sisters
    return [] unless mother.present?

    siblings.where(sex: SEX_FEMALE)
  end

  def children
    relation_by_sex = (sex == SEX_MALE ? { father: self } : { mother: self })
    Person.where relation_by_sex
  end

  def daughters
    children.where(sex: SEX_FEMALE)
  end

  def sons
    children.where(sex: SEX_MALE)
  end

  def siblings_in_law
    spouse.siblings
  end

  def brothers_in_law
    siblings_in_law.where(sex: SEX_MALE)
  end

  def sisters_in_law
    siblings_in_law.where(sex: SEX_FEMALE)
  end

  def parental_siblings
    maternal_siblings + paternal_siblings
  end

  def uncles
    maternal_uncles + paternal_uncles
  end

  def aunts
    maternal_aunts + paternal_aunts
  end

  def maternal_siblings
    mother.siblings
  end

  def paternal_siblings
    father.siblings
  end

  def maternal_uncles
    maternal_siblings.male
  end

  def maternal_aunts
    father.siblings.female
  end

  def paternal_uncles
    paternal_siblings.male
  end

  def paternal_aunts
    paternal_siblings.female
  end
end
