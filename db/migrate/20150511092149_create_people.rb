class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :sex
      t.integer :father_id
      t.integer :mother_id
      t.integer :spouse_id

      t.timestamps null: false
    end

    add_index :people, :father_id
    add_index :people, :mother_id
    add_index :people, :spouse_id
  end
end
