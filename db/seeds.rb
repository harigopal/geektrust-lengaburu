# Root generation
shan = Person.male! 'shan'
anga = Person.female! 'anga', spouse: shan

# Generation 1
ish = Person.male! 'ish', father: shan, mother: anga
chit = Person.male! 'chit', father: shan, mother: anga
ambi = Person.female! 'ambi', spouse: chit
vich = Person.male! 'vich', father: shan, mother: anga
lika = Person.female! 'lika', spouse: vich
satya = Person.female! 'satya', father: shan, mother: anga
vyan = Person.male! 'vyan', spouse: satya

# Generation 2
drita = Person.male! 'drita', father: chit, mother: ambi
jaya = Person.female! 'jaya', spouse: drita
vrita = Person.male! 'vrita', father: chit, mother: ambi
vila = Person.male! 'vila', father: vich, mother: lika
janki = Person.female! 'janki', spouse: vila
chika = Person.female! 'chika', father: vich, mother: lika
kapila = Person.male! 'kapila', spouse: chika
satvy = Person.female! 'satvy', father: vyan, mother: satya
asva = Person.male! 'asva', spouse: satvy
savya = Person.male! 'savya', father: vyan, mother: satya
kripi = Person.female! 'kripi', spouse: savya
saayan = Person.male! 'saayan', father: vyan, mother: satya
mina = Person.female! 'mina', spouse: saayan

# Generation 3
jata = Person.male! 'jata', father: drita, mother: jaya
driya = Person.female! 'driya', father: drita, mother: jaya
manu = Person.male! 'manu', spouse: driya
lavanya = Person.female! 'lavanya', father: vila, mother: janki
gru = Person.male! 'gru', spouse: lavanya
kriya = Person.male! 'kriya', father: savya, mother: kripi
misa = Person.male! 'misa', father: saayan, mother: mina
