Rails.application.routes.draw do
  get '/person/:name/:relation', to: 'person#relations'
  get '/person/:name/relation_to/:relation_name', to: 'person#relation_to'
  get '/mothers_with_most_girl_children', to: 'person#mothers_with_most_girl_children'
  root to: 'home#index'
end
