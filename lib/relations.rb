# Common functionality for managing relations.
class Relations
  # Gender-aware conversions for male relations.
  MALE_RELATIONS = {
    parent: :father,
    child: :son,
    spouse: :husband,
    sibling: :brother,
    parent_sibling: :uncle,
    nibling: :nephew,
    grandparent: :grandfather,
    grandchild: :grandson,
    sibling_in_law: :brother_in_law,
    child_in_law: :son_in_law,
    parent_in_law: :father_in_law
  }

  # Gender-aware conversions for female relations.
  FEMALE_RELATIONS = {
    parent: :mother,
    child: :daughter,
    spouse: :wife,
    sibling: :sister,
    parent_sibling: :aunt,
    nibling: :niece,
    grandparent: :grandmother,
    grandchild: :granddaugter,
    sibling_in_law: :sister_in_law,
    child_in_law: :daugther_in_law,
    parent_in_law: :mother_in_law
  }

  # Gender-neutral relations, used to reduce relation chains. More nested relations could be added for better
  # reductions.
  HASH_REDUCTION = {
    parent: {
      _value: :parent,
      parent: { _value: :grandparent },
      sibling: {
        _value: :parent_sibling,
        spouse: { _value: :parent_sibling },
        child: { _value: :cousin }
      },
    },
    sibling: {
      _value: :sibling,
      child: { _value: :nibling },
      spouse: { _value: :sibling_in_law }
    },
    child: {
      _value: :child,
      child: { _value: :grandchild },
      spouse: { _value: :child_in_law }
    },
    spouse: {
      _value: :spouse,
      sibling: { _value: :sibling_in_law },
      parent: { _value: :parent_in_law }
    }
  }

  class << self
    # Return male version of gender-neutral relation.
    def male_version(relation)
      MALE_RELATIONS[relation] || relation
    end

    # Return female version of gender-neutral relation.
    def female_version(relation)
      FEMALE_RELATIONS[relation] || relation
    end

    # Return gender-neutral version of any given relation.
    def gender_neutral_version(relation)
      # We obtain a gender neutral version by inverting male and female specific values, and merging the hashes.
      MALE_RELATIONS.invert.merge(FEMALE_RELATIONS.invert)[relation] || relation
    end

    # Let's use a short-hand.
    alias_method :gnv, :gender_neutral_version

    # Return a human-readable string from a given relation chain.
    #
    # @param relation_chain [Array] The chain of relation, of the form [:self, ...]
    # @return String
    def reduce(relation_chain, relation_gender)
      chain = relation_chain.dup

      # Let's get rid of :self from the chain. We need it only indirectly.
      chain.delete(:self)

      if chain.length == 0
        # Now, If chain's length is zero, then we're talking about the same person.
        'Same person'
      else
        reducer = HASH_REDUCTION.dup
        last_reduction = nil

        paternal_prefix = if chain.length > 1
          if chain.first == :mother
            'Maternal'
          elsif chain.first == :father
            'Paternal'
          end
        end

        while chain.present?
          next_in_chain = chain.shift
          reducer = reducer[gnv next_in_chain]

          if reducer.nil?
            chain.unshift next_in_chain
            break
          end

          last_reduction = reducer[:_value]
        end

        humanize last_reduction, chain, relation_gender, paternal_prefix
      end
    end

    private

    def humanize(reduction, remaining_chain, gender, paternal_prefix)
      main_reduction = if reduction
        if gender == Person::SEX_MALE
          male_version(reduction)
        else
          female_version(reduction)
        end.to_s.capitalize.gsub('_', '-')
      end

      sentence = (([main_reduction] + remaining_chain.map(&:to_s)) - [nil]).join "'s "

      paternal_prefix ? "#{paternal_prefix} #{sentence}" : sentence
    end
  end
end
