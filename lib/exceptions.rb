class Exceptions
  class APIError < StandardError
    def error_name
      self.class.name.demodulize
    end
  end

  class PersonNotFound < APIError; end
  class UnknownRelation < APIError; end
end
