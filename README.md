# Lengaburu

For a live demo, visit: http://lengaburu.harigopal.in

## Geektrust Problems

_Requirements_ for the problems solved by this app are available here: http://lengaburu.harigopal.in/GeekTrust-Problems.pdf

## Notes

I've solved all of the problems. The first three were trivial, and the last took me a few days. I'm not entirely happy
with the quality of the output for Problem 4, but I think it's a decent start.

To execute in your system:

1. Install Ruby 2.3.1
2. Bundle Install (requires support for SQLite3)
3. Set up the database with `rake db:setup`
4. Run server with `rails server`
5. Visit http://localhost:3000

Most of the code _of interest_ is in one of two files:

* `lib/relations.rb`
* `app/models/person.rb`

Also, I didn't bother with writing specs for the code, or documenting _all_ methods and constants because it would have
taken too long, and I'm not concerned with the _robustness_ of the code at the moment.

## Issues

There were a few issues with the problem statement, specifically spelling inconsistencies in the naming of citizens of
Lengaburu. I've modified the following names to include what seems to be missing vowels:

*  Jnki is now **Janki**
*  Lavnya is now **Lavanya**
*  Kpila is now **Kapila**
*  Krpi is now **Kripi**
*  Mnu is now **Manu**

See db/seeds.rb for definitive list.
